#include "MyRectangle.h"

#define CIMGGIP_MAIN
#include "CImgGIP05Mock.h"

MyRectangle::MyRectangle() : x1(0), y1(0), x2(20), y2(20)
{
	//
}

MyRectangle::MyRectangle(int x1, int y1, int x2, int y2)
{
	set(x1, y1, x2, y2);
}

MyRectangle::~MyRectangle()
{
	//
}

int MyRectangle::getX1()
{
	return x1;
}

int MyRectangle::getY1()
{
	return y1;
}

int MyRectangle::getX2()
{
	return x2;
}

int MyRectangle::getY2()
{
	return y2;
}

void MyRectangle::set(int x1, int y1, int x2, int y2)
{
	if (x2 < x1)
	{
		this->x1 = x2;
		this->x2 = x1;
	}
	else
	{
		this->x1 = x1;
		this->x2 = x2;
	}
	if (y2 < y1)
	{
		this->y1 = y2;
		this->y2 = y1;
	}
	else
	{
		this->y1 = y1;
		this->y2 = y2;
	}
}

void MyRectangle::draw()
{
	gip_draw_rectangle(x1, y1, x2, y2, blue);
}

bool MyRectangle::does_not_collide_with(const MyRectangle & other)
{
	return ((this->x1 > other.x2) || (this->y1 > other.y2)) || ((this->x2 < other.x1) || (this->y2 < other.y1));
}
