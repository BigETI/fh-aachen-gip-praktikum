#include <iostream>
#include <string>

using namespace std;

char tl(char c)
{
	return ((c >= 'A') && (c <= 'Z')) ? (c | 0x20) : c;
}

int main(int argc, char *argv[])
{
	string s, f, r;
	bool hw = true;
	int i;
	do
	{
		hw = false;
		cout << "Text = ? ";
		getline(cin, s);
		for (i = 0; i < s.length(); i++)
		{
			if ((tl(s[i]) < 'a') || (tl(s[i]) > 'z'))
			{
				hw = true;
				break;
			}
		}
		if (!hw)
		{
			for (i = 0; i < s.length(); i++)
				f += tl(s[i]);
			for (i = f.length() - 1; i >= 0; i--)
				r += f[i];
			if (f == r)
				cout << "Das eingegebene Wort ist ein Palindrom.\n";
			else
				cout << "Das eingegebene Wort ist KEIN Palindrom.\n";
		}
	} while (hw);
	system("PAUSE");
	return 0;
}