#include <iostream>

#define ARR_LEN	5

using namespace std;

int main(int argc, char *argv[])
{
	int arr[ARR_LEN];
	int i, j;
	for (i = 0; i < ARR_LEN; i++)
	{
		do
		{
			cout << "Bitte geben Sie die " << (i + 1) << ". Zahl ein: ? ";
			cin >> arr[i];
		} while ((arr[i] < 0) || (arr[i] > 9));
	}
	for (j = 9; j >= 0; j--)
	{
		cout << j;
		for (i = 0; i < ARR_LEN; i++)
		{
			if (arr[i] > j)
				cout << '.';
			else if (arr[i] < j)
				cout << ' ';
			else
				cout << '*';
		}
		cout << endl;
	}
	system("PAUSE");
}