#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	double aktuelle_eingabe = 0.0, vorherige_eingabe = 0.0, vorvorherige_eingabe = 0.0, a, gesamt = 0.0;
	do
	{
		cout << "Bitte geben Sie den neuen letzten Verbrauchswert ein: ? ";
		cin >> a;
		if (int(a) == 99)
			break;
		else if (a >= 0.0)
		{
			vorvorherige_eingabe = vorherige_eingabe;
			vorherige_eingabe = aktuelle_eingabe;
			aktuelle_eingabe = a;
			gesamt += a;
			cout << "Der bisherige Gesamtverbrauch betraegt " << gesamt <<
				"\nDer Durchschnitt der letzten drei Verbrauchswerte betraegt " << ((a + vorherige_eingabe + vorvorherige_eingabe) / 3.0) << endl;
		}
	} while (true /*int(aktuelle_eingabe) != 99*/);
	system("PAUSE");
	return 0;
}