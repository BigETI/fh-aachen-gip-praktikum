#include <iostream>
#include <string>
#include <fstream>

using namespace std;

// Personendatetn Struktur
struct Person
{
	string vorname, nachname, geburtstag;
};

// Personendaten aus einem String auslesen
Person extrahiere_person(string person_str)
{
	enum ParsePersonEnum { VORNAME, NACHNAME, GEBURTSTAG } state(VORNAME);
	Person ret;
	int i;
	char c;
	//ret.nachname
	for (i = 0; i < person_str.length(); i++)
	{
		c = person_str[i];
		switch (c)
		{
		case ' ':
			break;
		case ',':
			state = (ParsePersonEnum)(state + 1);
			break;
		default:
			switch (state)
			{
			case VORNAME:
				ret.vorname += c;
				break;
			case NACHNAME:
				ret.nachname += c;
				break;
			case GEBURTSTAG:
				ret.geburtstag += c;
				break;
			}
			break;
		}
	}
	return ret;
}

// Personenliste Struktur
struct PersonList
{
	Person data;
	PersonList *next = nullptr;
};

// Fuegt eine Person in die Personenliste hinzu
PersonList *addPersonListElement(PersonList *person_list, Person &person)
{
	PersonList *ret = nullptr;
	if (person_list->next)
		ret = addPersonListElement(person_list->next, person);
	else
	{
		ret = new PersonList();
		person_list->next = ret;
		ret->data = person;
	}
	return ret;
}

// L�scht alle nachkommenden Knoten aus der Liste
void clearPersonListChildren(PersonList *person_list)
{
	if (person_list->next)
		clearPersonListChildren(person_list->next);
	delete person_list->next;
	person_list->next = nullptr;
}

// Wandelt einen ifstream in einen String um
string ifstreamToString(ifstream &ifs)
{
	string ret;
	string l;
	// try
	do
	{
		if (getline(ifs, l));
			ret += l + "\n";
	} while (!(ifs.eof()));

	// catch
	return ret;
}

// Wandelt einen ifstream in eine Personenliste um
PersonList ifstreamToPersonList(ifstream &ifs)
{
	PersonList ret, *curr = nullptr;
	Person t;
	string l;
	ret.next = nullptr;
	do
	{
		if (getline(ifs, l))
		{
			t = extrahiere_person(l);
			if (curr)
				curr = addPersonListElement(curr, t);
			else
			{
				curr = &ret;
				ret.data = t;
			}
		}
	} while (!(ifs.eof()));
	return ret;
}

int main(int argc, char *argv[])
{
	ofstream output_fs;
	// try

	// Lese Vorlagendatei
	ifstream template_ifs("webseite.html.tmpl"), pdata_ifs("personendaten.txt");
	string template_str = ifstreamToString(template_ifs);
	PersonList pdata_list(ifstreamToPersonList(pdata_ifs));
	char c;
	int i;
	template_ifs.close();
	pdata_ifs.close();

	// Erstelle Website
	output_fs.open("website.html");

	// Ersetze alle % und $
	for (i = 0; i < template_str.length(); i++)
	{
		c = template_str[i];
		switch (c)
		{
		case '%':
			// <b>Nachname</b>, Vorname<br />\n
			for (PersonList *l = &pdata_list; l != nullptr; l = l->next)
				output_fs << "<b>" << l->data.nachname << "</b>, " << l->data.vorname << "<br />\n";
			break;
		case '$':
			// <b>Vorname Nachname</b>, Geburtstag<br />\n
			for (PersonList *l = &pdata_list; l != nullptr; l = l->next)
				output_fs << "<b>" << l->data.vorname << ' ' << l->data.nachname << "</b>, " << l->data.geburtstag << "<br />\n";
			break;
		default:
			output_fs << c;
		}
	}

	// Schlie�e Website
	output_fs.close();

	// Speicher freigeben
	clearPersonListChildren(&pdata_list);

	// catch
	return 0;
}