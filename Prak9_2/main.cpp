#include <iostream>
#include <string>

using namespace std;

// Zeichenkette suchen
int zeichenkette_suchen_rekursiv(string text, string zkette, unsigned int text_pos = 0, unsigned int text_such_pos = 0, unsigned int zkette_such_pos = 0)
{
	int ret = -1;

	// Wenn der innere Vergleich abgeschlossen wurde
	if (zkette_such_pos >= zkette.length())
		ret = text_pos;

	// Wenn noch im Vergleich
	else if (text_such_pos < text.length())
	{
		// Wenn beide Zeichen identisch sind
		if (text[text_such_pos] == zkette[zkette_such_pos])
			ret = zeichenkette_suchen_rekursiv(text, zkette, text_pos, text_such_pos + 1, zkette_such_pos + 1);

		// Sonst
		else
			ret = zeichenkette_suchen_rekursiv(text, zkette, text_pos + 1, text_pos + 1);
	}
	return ret;
}

int main(int argc, char *argv[])
{
	string t, st;
	int result;
	cout << "Bitte geben Sie den Text ein: ";
	getline(cin, t);
	cout << "Bitte geben Sie die zu suchende Zeichenkette ein: ";
	getline(cin, st);
	result = zeichenkette_suchen_rekursiv(t, st);
	if (result != -1)
		cout << "Die Zeichenkette \'" << st << "\' ist in dem Text \'" << t << "\' enthalten.\nSie startet ab Zeichen " << result << " (bei Zaehlung ab 0).\n";
	else
		cout << "Die Zeichenkette \'" << st << "\' ist NICHT in dem Text \'" << t << "\' enthalten.\n";
	system("PAUSE");
	return 0;
}