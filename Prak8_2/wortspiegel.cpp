#include "wortspiegel.h"

using namespace std;

void wortspiegel(string &text, int pos)
{
	int b, i;
	char c;
	string t;
	if ((pos >= 0) && (pos < text.length()))
	{
		c = text[pos];
		if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')))
		{
			// Im Wort
			for (b = pos; b >= 0; b--)
			{
				c = text[b];
				if (((c < 'a') || (c > 'z')) && ((c < 'A') || (c > 'Z')))
				{
					// Kein Wort mehr
					break;
				}
			}
			++b;

			// "b" beschreibt die Anfangspoition
			for (i = b; i < text.length(); i++)
			{
				c = text[i];
				if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')))
					t += c;
				else
					break;
			}

			for (i = (t.length() - 1); i >= 0; i--)
			{
				text[b + i] = t[(t.length() - 1) - i];
			}
		}
	}
}