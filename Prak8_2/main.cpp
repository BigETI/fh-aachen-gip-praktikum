#include <iostream>
#include "wortspiegel.h"

using namespace std;

char tl(char c)
{
	return ((c >= 'A') && (c <= 'Z')) ? (c | 0x20) : c;
}

int main(int argc, char *argv[])
{
	bool running = true;
	string text;
	char cmd;
	int pos = 0, i;
	cout << "Bitte geben Sie den Text ein: ? ";
	getline(cin, text);
	while (running)
	{
		cout << "Befehl (l: links, r: rechts, s: spiegeln, q: Ende) ?- ";
		cin >> cmd;
		switch (tl(cmd))
		{
		case 'l':
			if (pos > 0)
				--pos;
			break;
		case 'r':
			if (pos < (text.length() - 1))
				++pos;
			break;
		case 's':
			wortspiegel(text, pos);
			break;
		case 'q':
			running = false;
			break;
		}
		if (running)
		{
			cout << endl << text << endl;
			for (i = 0; i <= pos; i++)
			{
				if (pos == i)
					cout << "*\n";
				else
					cout << ' ';
			}
		}
	}
	system("PAUSE");
	return 0;
}