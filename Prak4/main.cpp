#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
	// Cesaer Verschluesselung
	/*string input;
	int count;
	int c, o;
	cout << "Bitte geben Sie den zu verschluesselnden Text ein: ";
	getline(cin, input);
	cout << "Bitte geben Sie die Anzahl Verschiebepositionen ein (als positive ganze Zahl): ";
	cin >> count;
	count %= 26;
	for (int i = 0; i < input.length(); i++)
	{
	c = input[i];
	o = c + count;
	if ((c >= 'a') && (c <= 'z'))
	{
	if (o > 'z')
	o = o - 'z' + '`';
	else if (o < 'a')
	o = o + 'z' - '`';
	}
	else if ((c >= 'A') && (c <= 'Z'))
	{
	if (o > 'Z')
	o = o - 'Z' + '@';
	else if (o < 'A')
	o = o + 'Z' - '@';
	}
	else
	o = c;
	cout << char(o);
	}
	cout << endl;*/

	// Busfahrplan
	int stunden, minuten, taktzeit_minuten;
	do
	{
		cout << "Bitte geben Sie die Stunden der Startuhrzeit ein: ";
		cin >> stunden;
	} while ((stunden < 0) || (stunden > 23));
	do
	{
		cout << "Bitte geben Sie die Minuten der Startuhrzeit ein: ";
		cin >> minuten;
	} while ((minuten < 0) || (minuten > 59));
	cout << "Der erste Bus faehrt also um " << stunden << ':' << minuten << " Uhr.\n";
	do
	{
		cout << "Bitte geben Sie die Taktzeit in Minuten ein: ";
		cin >> taktzeit_minuten;
	} while ((taktzeit_minuten < 0) || (taktzeit_minuten > 180));
	for (int i = (stunden * 60) + minuten, stunde = -1; i <= 1440; i += taktzeit_minuten)
	{
		if (stunde != -1)
		{
			if (((i / 60) != stunde))
				cout << endl;
			else
				cout << " ";
		}
		stunde = i / 60;
		cout << stunde << ':' << (i % 60);
	}
	cout << endl;
	system("PAUSE");
	return 0;
}