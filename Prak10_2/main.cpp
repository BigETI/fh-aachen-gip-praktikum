#include <iostream>
using namespace std;
struct TListenKnoten
{
	int data;
	TListenKnoten *next, *prev;
};
void hinten_anfuegen(TListenKnoten* &anker, int wert)
{
	TListenKnoten *neuer_eintrag = new TListenKnoten;
	neuer_eintrag->data = wert;
	neuer_eintrag->next = nullptr;
	neuer_eintrag->prev = nullptr;
	if (anker == nullptr)
		anker = neuer_eintrag;
	else
	{
		TListenKnoten *ptr = anker;
		while (ptr->next != nullptr)
			ptr = ptr->next;
		ptr->next = neuer_eintrag;
		neuer_eintrag->prev = ptr;
	}
}
void liste_ausgeben(TListenKnoten * anker)
{
	if (anker == nullptr)
		cout << "Leere Liste." << endl;
	else
	{
		cout << "[ ";
		TListenKnoten *ptr = anker;
		do
		{
			cout << ptr->data;
			if (ptr->next != nullptr) cout << " , ";
			else cout << " ";
			ptr = ptr->next;
		} while (ptr != nullptr);
		cout << "]" << endl;
	}
}
void liste_ausgeben_rueckwaerts(TListenKnoten* anker)
{
	if (anker == nullptr)
		cout << "Leere Liste." << endl;
	else
	{
		while (anker->next)
			anker = anker->next;
		cout << "[ ";
		TListenKnoten *ptr = anker;
		do
		{
			cout << ptr->data;
			if (ptr->prev != nullptr) cout << " , ";
			else cout << " ";
			ptr = ptr->prev;
		} while (ptr != nullptr);
		cout << "]" << endl;
	}
}
void einfuegen(TListenKnoten* &anker, int wert_neu, int vor_wert)
{
	TListenKnoten *a, *curr_anker, *last_anker = anker;
	for (curr_anker = anker; curr_anker != nullptr; curr_anker = curr_anker->next)
	{
		last_anker = curr_anker;
		if (curr_anker->data == vor_wert)
		{
			a = new TListenKnoten();
			a->data = wert_neu;
			a->next = curr_anker;
			a->prev = curr_anker->prev;
			if (curr_anker->prev)
				curr_anker->prev->next = a;
			curr_anker->prev = a;
			break;
		}
	}
	if (curr_anker == nullptr)
		hinten_anfuegen(last_anker, wert_neu);
	while (anker->prev)
		anker = anker->prev;
}
int main()
{
	/*int laenge = 10;
	TListenKnoten *anker = nullptr;
	liste_ausgeben_rueckwaerts(anker);
	for (int i = 0; i < laenge; i++)
		hinten_anfuegen(anker, i*i);
	liste_ausgeben_rueckwaerts(anker);
	system("PAUSE");
	return 0;*/

	int laenge = 10;
	TListenKnoten *anker = nullptr;
	for (int i = 0; i < laenge; i++)
		hinten_anfuegen(anker, i*i);
	liste_ausgeben(anker);
	cout << endl << endl;
	liste_ausgeben_rueckwaerts(anker);
	int wert_neu, vor_wert;
	cout << "Einzufuegender Wert: "; cin >> wert_neu;
	cout << "Vor welchem Wert? "; cin >> vor_wert;
	einfuegen(anker, wert_neu, vor_wert);
	liste_ausgeben(anker);
	cout << endl << endl;
	liste_ausgeben_rueckwaerts(anker);
	system("PAUSE");
	return 0;
}