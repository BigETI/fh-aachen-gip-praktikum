#include <iostream>
#define CIMGGIP_MAIN
#include "CImgGIP04.h"
using namespace std;
using namespace cimg_library;
int main()
{
	int mx, my;
	gip_color mouse_color = { 0, 0, 0 };
	while (gip_window_not_closed())
	{
		mx = gip_mouse_x();
		my = gip_mouse_y();
		gip_load_image("Campus_Eupener_Strasse.bmp");
		if ((mx >= 0) && (mx < gip_win_size_x) && (my >= 0) && (my < gip_win_size_y))
			gip_get_color(mx, my, mouse_color);
		gip_draw_circle(mx, my, 50, mouse_color);
		gip_wait(300);
	}
	return 0;
}