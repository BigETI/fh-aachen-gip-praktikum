#include <iostream>
#define CIMGGIP_MAIN
#include "CImgGIP05.h"
using namespace std;
using namespace cimg_library;
int main()
{
	struct color_backup { int x, y; int R, G, B; };
	const unsigned int max_number = 50000;
	color_backup my_array[max_number];
	int i = 0, x, y;
	gip_color col;
	gip_load_image("Campus_Eupener_Strasse.bmp");
	gip_stop_updates();
	// Phase 1 ...
	for (i = 0; i < max_number; i++)
	{
		x = gip_random(0, gip_win_size_x - 1);
		y = gip_random(0, gip_win_size_y - 1);
		my_array[i].x = x;
		my_array[i].y = y;
		my_array[i].R = gip_get_red(x, y);
		my_array[i].G = gip_get_green(x, y);
		my_array[i].B = gip_get_blue(x, y);
	}
	// Phase 2 ...
	for (i = 0; i < max_number; i++)
	{
		col[0] = my_array[i].R;
		col[1] = my_array[i].G;
		col[2] = my_array[i].B;
		gip_draw_circle(my_array[i].x, my_array[i].y, 5, col);
	}
	gip_start_updates();
	system("PAUSE");
	return 0;
}