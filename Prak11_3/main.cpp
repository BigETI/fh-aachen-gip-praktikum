#include <iostream>
#define CIMGGIP_MAIN
#include "CImgGIP04.h"
using namespace std;
using namespace cimg_library;

int main()
{
	const int box_size = 30; // ... Groesse der einzelnen Kaestchen
	//const int box_size = 4; // ... Groesse der einzelnen Kaestchen
	const int border = 20; // ... Rand links und oben bis zu den ersten Kaestchen
	const int grid_size = 10;
	//const int grid_size = 50;
	int grid[grid_size][grid_size] = { 0 };
	int next_grid[grid_size][grid_size] = { 0 };
	int i, j, lx, ly, rx, ry, nc;
	bool hlx, hly, hrx, hry;
	for (i = 0; i < grid_size; i++)
	{
		for (j = 0; j < grid_size; j++)
		{
			grid[i][j] = gip_random(0, 1);
		}
	}
	while (gip_window_not_closed())
	{
		// Spielfeld anzeigen ...
		gip_stop_updates();
		gip_white_background();
		for (i = 0; i < grid_size; i++)
		{
			for (j = 0; j < grid_size; j++)
			{
				if (grid[i][j])
					gip_draw_rectangle(border + (i * box_size), border + (j * box_size), border + ((i + 1) * box_size), border + ((j + 1) * box_size), blue);
			}
		}
		gip_start_updates();
		gip_sleep(3);
		//gip_wait(20);
		// Berechne das naechste Spielfeld ...
		// Achtung; F�r die Zelle (x,y) darf die Position (x,y) selbst *nicht*
		// mit in die Betrachtungen einbezogen werden.
		// Ausserdem darf bei zellen am rand nicht �ber den Rand hinausgegriffen
		// werden (diese Zellen haben entsprechend weniger Nachbarn) ...
		for (i = 0; i < grid_size; i++)
		{
			for (j = 0; j < grid_size; j++)
			{
				nc = 0;
				lx = i - 1;
				ly = j - 1;
				rx = i + 1;
				ry = j + 1;
				hlx = (i > 0);
				hly = (j > 0);
				hrx = (i < (grid_size - 1));
				hry = (j < (grid_size - 1));
				nc = (hlx ? ((hly ? grid[lx][ly] : 0) + (hry ? grid[lx][ry] : 0) + grid[lx][j]) : 0) +
					(hrx ? ((hly ? grid[rx][ly] : 0) + (hry ? grid[rx][ry] : 0) + grid[rx][j]) : 0) +
					(hly ? grid[i][ly] : 0) +
					(hry ? grid[i][ry] : 0);
				if (grid[i][j])
				{
					if ((nc < 2) || (nc > 3))
						next_grid[i][j] = 0;
				}
				else
				{
					if (nc == 3)
						next_grid[i][j] = 1;
				}
			}
		}
		// Kopiere das naechste Spielfeld in das aktuelle Spielfeld ...
		for (i = 0; i < grid_size; i++)
		{
			for (j = 0; j < grid_size; j++)
				grid[i][j] = next_grid[i][j];
		}
	}
	return 0;
}