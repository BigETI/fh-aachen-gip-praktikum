#pragma once
#include "MyRectangle.h"

class MyFilledRectangle : MyRectangle
{
private:
	MyFilledRectangle &operator=(const MyFilledRectangle &);
public:
	MyFilledRectangle();
	MyFilledRectangle(int x1, int y1, int x2, int y2);
	~MyFilledRectangle();
	bool does_not_collide_with(const MyFilledRectangle& other);
	void draw();
};