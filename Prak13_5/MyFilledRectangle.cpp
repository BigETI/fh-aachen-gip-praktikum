#include "MyFilledRectangle.h"

#include "CImgGIP05.h"

MyFilledRectangle::MyFilledRectangle() : MyRectangle()
{
	//
}

MyFilledRectangle::MyFilledRectangle(int x1, int y1, int x2, int y2) : MyRectangle(x1, y1, x2, y2)
{
	//
}

MyFilledRectangle::~MyFilledRectangle()
{
	//
}

void MyFilledRectangle::draw()
{
	MyRectangle::draw();
	int nx1 = x1 + 2, ny1 = y1 + 2, nx2 = x2 - 2, ny2 = y2 - 2;
	if (((x2 - x1) >= 2) && ((y2 - y1) >= 2))
	{
		for (int i = ny1; i <= ny2; i++)
			gip_draw_line(nx1, i, nx2, i, red);
	}
}

bool MyFilledRectangle::does_not_collide_with(const MyFilledRectangle & other)
{
	return ((this->x1 > other.x2) || (this->y1 > other.y2)) || ((this->x2 < other.x1) || (this->y2 < other.y1));
}