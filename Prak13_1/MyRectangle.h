#pragma once
class MyRectangle
{
private:
	int x1, y1, x2, y2;

	MyRectangle(const MyRectangle &);
	MyRectangle &operator=(const MyRectangle &);
public:
	MyRectangle();
	MyRectangle(int x1, int y1, int x2, int y2);
	~MyRectangle();

	int getX1();
	int getY1();
	int getX2();
	int getY2();
	void set(int x1, int y1, int x2, int y2);
	void draw();
};