void sortiere(int a[], int n)
{
	int t, i, j;
	for (i = 0, j; i != n; i++)
	{
		t = a[i];
		j = i;
		while (j > 0)
		{
			if (a[j - 1] > t)
			{
				a[j] = a[j - 1];
				--j;
			}
			else
				break;
		}
		a[j] = t;
	}
}