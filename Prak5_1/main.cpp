#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	for (int i = 0, j; i < 7; i++)
	{
		for (j = i; j < 7; j++)
		{
			if (j > 0)
				cout << ' ';
			cout << "+---+---+";
		}
		cout << endl;
		for (j = i; j < 7; j++)
		{
			if (j > 0)
				cout << ' ';
			cout << "| " << i << " | " << j << " |";
		}
		cout << endl;
		for (j = i; j < 7; j++)
		{
			if (j > 0)
				cout << ' ';
			cout << "+---+---+";
		}
		cout << endl;
	}
	system("PAUSE");
	return 0;
}