#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	// Vergleich von Datumsangaben

	/*int d1, d2, m1, m2, y1, y2;
	cout << "Bitte geben Sie den Tag des ersten Datums ein: ";
	cin >> d1;
	cout << "Bitte geben Sie den Monat des ersten Datums ein: ";
	cin >> m1;
	cout << "Bitte geben Sie das Jahr des ersten Datums ein: ";
	cin >> y1;
	cout << "Bitte geben Sie den Tag des zweiten Datums ein: ";
	cin >> d2;
	cout << "Bitte geben Sie den Monat des zweiten Datums ein: ";
	cin >> m2;
	cout << "Bitte geben Sie das Jahr des zweiten Datums ein: ";
	cin >> y2;
	bool same = (y2 == y1) && (m1 == m2) && (d1 == d2);
	bool before = false;
	if (same)
		cout << "Die Datumsangaben sind identisch.\n";
	else
	{
		if (y1 < y2)
			before = true;
		else if (y1 == y2)
		{
			if (m1 < m2)
				before = true;
			else if (m1 == m2)
			{
				if (d1 < d2)
					before = true;
			}
		}
		if (before)
			cout << "Das erste Datum liegt vor dem zweiten Datum.\n";
		else
			cout << "Das erste Datum liegt nach dem zweiten Datum.\n";
	}*/

	// Kleinste und gr��te Zahl, if-else

	int a, b, c, d, e, min_result = 0, max_result = 0, min_pos = 0, max_pos = 0;
	cout << "Bitte geben Sie die 1. Zahl ein: ";
	cin >> a;
	cout << "Bitte geben Sie die 2. Zahl ein : ";
	cin >> b;
	cout << "Bitte geben Sie die 3. Zahl ein : ";
	cin >> c;
	cout << "Bitte geben Sie die 4. Zahl ein : ";
	cin >> d;
	cout << "Bitte geben Sie die 5. Zahl ein : ";
	cin >> e;
	min_result = a;
	min_pos = 1;
	if (b < min_result)
	{
		min_result = b;
		min_pos = 2;
	}
	if (c < min_result)
	{
		min_result = c;
		min_pos = 3;
	}
	if (d < min_result)
	{
		min_result = d;
		min_pos = 4;
	}
	if (e < min_result)
	{
		min_result = e;
		min_pos = 5;
	}
	max_result = a;
	max_pos = 1;
	if (b > max_result)
	{
		max_result = b;
		max_pos = 2;
	}
	if (c > max_result)
	{
		max_result = c;
		max_pos = 3;
	}
	if (d > max_result)
	{
		max_result = d;
		max_pos = 4;
	}
	if (e > max_result)
	{
		max_result = e;
		max_pos = 5;
	}
	cout <<
		"Die " << min_pos << ". Zahl war die kleinste der eingegebenen Zahlen und lautet : " <<
		min_result <<
		"\nDie " << max_pos << ". Zahl war die groesste der eingegebenen Zahlen und lautet : " <<
		max_result << endl;
	system("PAUSE");
	return 0;
}