#include <iostream>
#define CIMGGIP_MAIN
#include "CImgGIP04.h"
using namespace std;
using namespace cimg_library;

int main()
{
	int mx, my, i, j;
	gip_color black_color = { 0, 0, 0 };
	gip_color grey_color = { 0, 0, 0 };
	while (gip_window_not_closed())
	{
		gip_stop_updates();
		mx = gip_mouse_x();
		my = gip_mouse_y();
		gip_load_image("Campus_Eupener_Strasse.bmp");
		for (i = 0; i < gip_win_size_x; i++)
		{
			for (j = 0; j < gip_win_size_y; j++)
			{
				if (gip_distance(mx, my, i, j) > 100)
				{
					gip_set_color(i, j, black_color);
				}
				else
					gip_set_grey(i, j, gip_get_grey(i, j));
			}
		}
		gip_start_updates();
		gip_sleep(3);
	}
	return 0;
}